Página implementada durante mi periodo como monitora del programa de Ingeniería de Sistemas de la Universidad del Valle sede tuluá, la página fué desarrollada con el propósito de alojar las memorias del IX Encuentro Departamental de Semilleros de Investigación realizado en Univalle en la sede tuluá en 2019.

Html5, css, bootstrap.

Puedes verla [aquí](https://invessoft.com/ponencias2019/poster.html)
